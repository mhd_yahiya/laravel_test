<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;

class UserController extends Controller
{
    public function createAccount(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'confirm_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            $data = [
                'code' => 401,
                'status' => $validator->errors()
            ];
        } else {
            if (User::where('email', $request->email)->exists()) {
                $data = [
                    'code' => 401,
                    'status' => 'User already exists'
                ];
            } else {
                $input = $request->all();
                $input['password'] = bcrypt($input['password']);

                $user = User::create($input);
                $success['token'] =  $user->createToken('MyApp')->accessToken;
                $success['name'] =  $user->name;

                $data = [
                    'code' => 200,
                    'status' => $success
                ];
            }
        }

        return response()->json($data, $data['code']);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'bail|required|email|exists:users',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            $data = [
                'code' => 401,
                'status' => $validator->errors()
            ];
        } else {
            $isAuth = Auth::attempt(['email' => $request->email, 'password' => $request->password]);

            if ($isAuth) {
                $user = Auth::user();

                $success['token'] =  $user->createToken('MyApp')->accessToken;

                $data = [
                    'code' => 200,
                    'status' => $success
                ];
            } else {
                $data = [
                    'code' => 401,
                    'status' => 'Unauthorised'
                ];
            }
        }

        return response()->json($data, $data['code']);
    }

    public function fetchUserData()
    {
        $user = Auth::user();

        if ($user) {
            $data = [
                'code' => 200,
                'status' => $user
            ];
        } else {
            $data = [
                'code' => 404,
                'status' => 'Not found.'
            ];
        }

        return response()->json($data, $data['code']);
    }
}
