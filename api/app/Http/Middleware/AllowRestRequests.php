<?php

namespace App\Http\Middleware;

use Closure;

class AllowRestRequests
{
    public function handle($request, Closure $next)
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Origin, Content-Type, x-autovert-token, x-socket-id, *');

        return $next($request);
    }
}
